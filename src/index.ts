import {
  Grid,
  Indoor,
  Link,
  MODES,
  PointOfInterest,
  Annotation,
  setDraggable,
  ImperialRulerDrawFunction,
  MetricRulerDrawFunction,
  CURSOR,
  createFloorPlan,
} from '@3i-web/indoorjs';
import Konva from 'konva';

let destroyGrid = undefined as undefined | (() => void);
async function createGrid() {
  if (destroyGrid !== undefined) return; // Prevents multi-instance of the map

  /////////////////////////////
  // Start of IndoorJS logic //
  /////////////////////////////

  // 1. Creates the Indoor instance
  const indoor = new Indoor('.indoor');
  indoor.setMode(MODES.MOVE_ONLY); // Change the interaction mode to move only

  // 2. Creates the grid behind
  const grid = new Grid(undefined, {
    stroke: 'lightblue',
    opacity: 0.5,
  });
  indoor.setGrid(grid);
  grid.autoUnitScale(true); // Makes the grid scale with zoom

  // 3. Creates X and Y axes
  const yAxis = indoor.addAxis({
    color: 'blue',
    opacity: 1,
    orientation: 'vertical',
  });
  const xAxis = indoor.addAxis({
    color: 'green',
    opacity: 1,
    orientation: 'horizontal',
  });
  indoor.bringToTop('axes'); // shows axes on top of other Indoor elements
  xAxis.autoUnitScale(true); // Makes the axis scale with zoom
  yAxis.autoUnitScale(true); // Makes the axis scale with zoom

  // 4. Makes IndoorJS instance automatically resize to parent container
  let firstResize = true;
  indoor.autoFitToParent('.indoor-container', () => {
    if (firstResize) indoor.center();
    firstResize = false;
  });

  // 5. Creates 2 POI : Point of Interest
  const poi = new PointOfInterest({ x: 10, y: 10 }, '1');
  const poi2 = new PointOfInterest({ x: 17, y: 15 }, '2');
  // poi.draggable(true); // use this to make the POI draggable
  indoor.add(poi);
  indoor.add(poi2);

  // 6. Add a link between the 2 POI
  const link = new Link({
    start: poi,
    end: poi2,
    lineConfig: {
      strokeWidth: 0.02,
      stroke: 'green',
    },
  });
  indoor.add(link);

  // 7. Add clicking logic for the POI and stage
  poi.on('click', (ev) => {
    // Logic to make the POI show a vision Radar after it is clicked
    if (!ev.evt.shiftKey) indoor.clearSelection();
    if (indoor.getSelection().length === 0) {
      poi.setColor('red');
      poi.showVision(Math.floor(Math.random() * 360));
      poi2.setColor('green');
      poi2.hideVision();
    }
    indoor.addToSelection(poi);
  });

  poi2.on('click', (ev) => {
    // Logic to make the POI show a vision Radar after it is clicked
    if (!ev.evt.shiftKey) indoor.clearSelection();
    if (indoor.getSelection().length === 0) {
      poi2.setColor('red');
      poi2.showVision(Math.floor(Math.random() * 360));
      poi.setColor('green');
      poi.hideVision();
    }
    indoor.addToSelection(poi2);
    poi2.showVision(0);
  });

  indoor.onClick((ev) => {
    // When clicking anywhere on the map, it unselects everything
    if (ev.target instanceof Konva.Stage) {
      poi.hideVision();
      poi.setColor('green');
      poi2.setColor('green');
      poi2.hideVision();
    }
    if (!(ev.target instanceof Konva.Stage) || indoor.getMode() !== MODES.MOVE_ONLY) return;
    indoor.clearSelection();
  });

  // 8. Add an anotation on the map
  const annotation = new Annotation({
    x: 10,
    y: 10,
    offsetY: 0.4,
    text: {
      text: 'Hello my friends',
      fontSize: 1,
      padding: 0.3,
      editable: true,
    },
    tag: {
      fill: '#bbb',
      stroke: '#333',
      strokeWidth: 0.1,
      lineJoin: 'round',
      pointerWidth: 0.3,
      pointerHeight: 0.3,
      pointerDirection: 'down',
      cornerRadius: 0.2,
    },
  });
  indoor.add(annotation);
  // Makes the annotation draggable.
  // Call cancelAnnotationDrag to cancel the behavior
  // See setDraggable utility on IndoorJS wiki
  const cancelAnnotationDrag = setDraggable(annotation, CURSOR.MOVE);

  // 9. Creates a FloorPlan image
  const floorPlan = await createFloorPlan(
    'https://online.visual-paradigm.com/repository/images/db69fb4f-09bb-4327-b7d9-7d6df4ae2eb5/floor-plan-design/house-floor-plan.png',
    undefined,
    { height: 419 * 0.2, width: 688 * 0.2 },
  );
  if (floorPlan !== undefined) {
    floorPlan.opacity(0.7); // Makes the FloorPlan transparent
    indoor.add(floorPlan);
    floorPlan.moveToBottom(); // Render the FloorPlan under the POI and Link
  }

  ///////////////////////////
  // End of IndoorJS logic //
  ///////////////////////////

  const centerBtn = document.getElementById('center');
  centerBtn!.onclick = () => {
    indoor.center();
  };

  const resetZoomBtn = document.getElementById('reset-zoom');
  resetZoomBtn!.onclick = () => {
    indoor.resetZoom();
  };

  const resetZoomAndCenterBtn = document.getElementById('reset-zoom-center');
  resetZoomAndCenterBtn!.onclick = () => {
    indoor.resetZoom();
    indoor.center();
  };

  const xRulerBtn = document.getElementById('x-ruler');
  xRulerBtn!.onclick = () => {
    if (indoor.getRuler()!.isAxisVisible('x')) indoor.getRuler()!.hideAxis('x');
    else indoor.getRuler()!.showAxis('x');
  };
  const yRulerBtn = document.getElementById('y-ruler');
  yRulerBtn!.onclick = () => {
    if (indoor.getRuler()!.isAxisVisible('y')) indoor.getRuler()!.hideAxis('y');
    else indoor.getRuler()!.showAxis('y');
  };

  const moveModeBtn = document.getElementById('mode-move');
  moveModeBtn!.onclick = () => {
    indoor.setMode(MODES.MOVE_ONLY);
  };

  const selectionModeBtn = document.getElementById('mode-selection');
  selectionModeBtn!.onclick = () => {
    indoor.setMode(MODES.SELECT_AND_MOVE);
  };

  const metricModeBtn = document.getElementById('mode-metric');
  metricModeBtn!.onclick = () => {
    const ruler = indoor.getRuler();
    if (ruler) {
      ruler.setDrawFunction(MetricRulerDrawFunction);
    }
  };
  const imperialModeBtn = document.getElementById('mode-imperial');
  imperialModeBtn!.onclick = () => {
    const ruler = indoor.getRuler();
    if (ruler) {
      ruler.setDrawFunction(ImperialRulerDrawFunction);
    }
  };

  destroyGrid = () => {
    indoor.destroy();
    destroyGrid = undefined;
    selectionModeBtn!.onclick = null;
    moveModeBtn!.onclick = null;
    yRulerBtn!.onclick = null;
    xRulerBtn!.onclick = null;
    resetZoomAndCenterBtn!.onclick = null;
    resetZoomBtn!.onclick = null;
    centerBtn!.onclick = null;
  };
}

const createGridBtn = document.getElementById('create-grid');
createGridBtn!.onclick = () => {
  createGrid();
};
const destroyGridBtn = document.getElementById('destroy-grid');
destroyGridBtn!.onclick = () => {
  if (destroyGrid !== undefined) destroyGrid();
};

# indoorjs-demo

Demo code for 3i-web/indoorjs package

![image](https://gitlab.com/3i-web/indoorjs/-/wikis/uploads/2d45462771e0d88be7b9fea2c70f44ca/image.png)

## Installation

First install dependencies with `npm install`

Then build the project using `npm run build`

Run the HTTP server using `npm run serve`

Go to `localhost:8080` to see the demo

